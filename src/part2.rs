use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::anychar;
use nom::combinator::{value, peek};
use nom::multi::many0;
use nom::IResult;
use trebuchet::get_lines;
use trebuchet::FileToParse::Input;

// if matches, the parse consume ON, but not E, as it can be used later for Eight.
fn one(input: &str) -> IResult<&str, Option<&str>> {
    let (input, _) = peek(tag("one"))(input)?;
    let (rest, _) = tag("on")(input)?;
    Ok((rest, Some("1")))
}

fn two(input: &str) -> IResult<&str, Option<&str>> {
    let (input, _) = peek(tag("two"))(input)?;
    let (rest, _) = tag("tw")(input)?;
    Ok((rest, Some("2")))
}

fn three(input: &str) -> IResult<&str, Option<&str>> {
    let (input, _) = peek(tag("three"))(input)?;
    let (rest, _) = tag("thr")(input)?;
    Ok((rest, Some("3")))
}

fn five(input: &str) -> IResult<&str, Option<&str>> {
    let (input, _) = peek(tag("five"))(input)?;
    let (rest, _) = tag("fiv")(input)?;
    Ok((rest, Some("5")))
}

fn seven(input: &str) -> IResult<&str, Option<&str>> {
    let (input, _) = peek(tag("seven"))(input)?;
    let (rest, _) = tag("seve")(input)?;
    Ok((rest, Some("7")))
}

fn eight(input: &str) -> IResult<&str,Option<&str>> {
    let (input, _) = peek(tag("eight"))(input)?;
    let (rest, _) = tag("eigh")(input)?;
    Ok((rest, Some("8")))
}

fn nine(input: &str) -> IResult<&str, Option<&str>> {
    let (input, _) = peek(tag("nine"))(input)?;
    let (rest, _) = tag("nin")(input)?;
    Ok((rest, Some("9")))
}

fn get_digit_or_junk(input: &str) -> IResult<&str, Option<&str>> {
    alt((
        one,
        two, 
        three,
        value(Some("4"), tag("four")),
        five,
        value(Some("6"), tag("six")),
        seven,
        eight,
        nine,
        value(Some("1"), tag("1")),
        value(Some("2"), tag("2")),
        value(Some("3"), tag("3")),
        value(Some("4"), tag("4")),
        value(Some("5"), tag("5")),
        value(Some("6"), tag("6")),
        value(Some("7"), tag("7")),
        value(Some("8"), tag("8")),
        value(Some("9"), tag("9")),
        value(Some("0"), tag("0")),
        value(None, anychar),
    ))(input)
}

fn extract2(input: &str) -> Option<u32> {
    let (_rest, tokens) = many0(get_digit_or_junk)(input).unwrap();
    let mut digits = tokens
        .into_iter()
        .filter(Option::is_some)
        .map(Option::unwrap)
        .peekable();

    let Some(l) = digits.peek() else { return None };
    let l = *l;
    let Some(r) = digits.last() else { return None };

    let num_str = format!("{}{}", l, r);
    num_str.parse().ok()
}

fn main() -> std::io::Result<()> {
    let lines = get_lines(Input)?;

    let result: u32 = lines
        .flat_map(|string| {
            extract2(&string)
        })
        .sum();

    println!("{}", result);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_extract2() {
        assert_eq!(29, extract2("two1nine").unwrap());
        assert_eq!(83, extract2("eightwothree").unwrap());
        assert_eq!(13, extract2("abcone2threexyz").unwrap());
        assert_eq!(24, extract2("xtwone3four").unwrap());
        assert_eq!(42, extract2("4nineeightseven2").unwrap());
        assert_eq!(14, extract2("zoneight234").unwrap());
        assert_eq!(76, extract2("7pqrstsixteen").unwrap());
        assert_eq!(88, extract2("eight").unwrap());
        assert_eq!(11, extract2("1").unwrap());
        assert_eq!(11, extract2("111111").unwrap());
        assert_eq!(21, extract2("twone").unwrap());
    }
}
