use trebuchet::get_lines;
use trebuchet::FileToParse::Input;

fn extract(input: &str) -> Option<u32> {
    let mut digits = input.chars().filter(|c| c.is_numeric()).peekable();

    let Some(l) = digits.peek() else { return None };
    let l = l.clone();
    let Some(r) = digits.last() else { return None };

    let num_str = format!("{}{}", l, r);
    num_str.parse().ok()
}

fn main() -> std::io::Result<()> {
    let lines = get_lines(Input)?;

    let result: u32 = lines.flat_map(|string| extract(&string)).sum();

    println!("{}", result);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_multiple() {
        let fixture = "a1b2c3d4e5f";
        assert_eq!(15u32, extract(fixture).unwrap());
    }

    #[test]
    fn test_parse_single() {
        let fixture = "treb7uchet";
        assert_eq!(77u32, extract(fixture).unwrap());
    }
}
